import React, { Component, Fragment } from 'react';
import Title from "./Title";
import { ProductsConsumer } from "../context";
import Product from "./Product";

class Products extends Component {

  render(){
    return (
      <Fragment>
        <div className="py-5">
          <div className="container">
            <Title title="Our products" />
            <div className="row">
              <ProductsConsumer>
                {(data) => (
                  data.products.map((product) => (
                    <Product product={product} key={product.id} />
                  ))
                )}
              </ProductsConsumer>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Products;

