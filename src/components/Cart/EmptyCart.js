import React, { Fragment } from 'react'
import Title from '../Title';

export default function EmptyCart() {
  return (
    <Fragment>
      <div className="py-5">
        <div className="container">
          <Title title="You have no products in your cart" />
        </div>
      </div>
    </Fragment>      
  )
}