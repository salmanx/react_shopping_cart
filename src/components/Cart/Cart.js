import React, { Component, Fragment } from 'react';
import Title from '../Title';
import CartColumns from "./CartColumn";
import EmptyCart from './EmptyCart';
import CartItems from "./CartItems";
import CartTotal from "./CartTotal";
import { ProductsConsumer } from '../../context';

class Cart extends Component {
  render(){
    return (
      <Fragment>
        <div className="py-5">
          <div className="container">
            <ProductsConsumer>
              {(data) => {
                if (data.carts.length > 0) {
                  return (
                    <Fragment>
                      <Title title="Your cart" />
                      <div className="row">
                        <CartColumns />
                        <CartItems  products={data} />
                        <CartTotal products={data} />
                      </div>                    
                    </Fragment>
                  )                  
                } else {
                  return (
                    <div className="row">                    
                      <EmptyCart />
                    </div>                                        
                  )
                }
              }}
            </ProductsConsumer>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Cart;

