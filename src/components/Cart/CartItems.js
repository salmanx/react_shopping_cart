import React, { Fragment } from 'react'
import CartItem from "./CartItem";

export default function CartItems({products}) {
  const { carts } = products;
  return (
    <Fragment>
      <div className="col-10">
        {carts.map((cart) => {
          return <CartItem cartProduct={cart} products={products} key={cart.id}/>
        })}
      </div>
    </Fragment>     
  )
}
