import React, { Component } from 'react'
import  styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ButtonContainer } from './Button';
import { ProductsConsumer } from '../context';

export default class Modal extends Component{
  render(){
    return (
      <ProductsConsumer>
      {(data) => {
        const { openModal, handleCloseModal } = data;
        const { title, img, price } = data.modalProduct;
        if (!openModal) {
          return null;
        } else {
          return (
            <ModalContainer>
              <div className="container">
                <div className="row">
                  <div
                    className="col-8 mx-auto col-md-6 col-lg-4 p-5 text-center text-capitalize" 
                    id="modal"
                  >
                    <h5>item added to cart</h5>
                    <img src={"assets/" + img} className="img-fluid" alt={title} />
                    <h5>{title}</h5>
                    <h5 className="text-muted">price : ${price}</h5>
                    <Link to="/">
                      <ButtonContainer
                        onClick={() => handleCloseModal()}
                      >
                        Continue Shopping
                      </ButtonContainer>
                    </Link>
                    <Link to="/cart">
                      <ButtonContainer
                        cart
                        onClick={() => handleCloseModal()}
                      >
                        Go To Cart
                      </ButtonContainer>
                    </Link>
                  </div>
                </div>
              </div>
            </ModalContainer>
          );
        }
      }}
      </ProductsConsumer>
    )
  }
}

const ModalContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.3);
  display: flex;
  align-items: center;
  justify-content: center;
  #modal {
    background: var(--mainWhite);
  }
`;