import React, { Component, Fragment } from 'react';

class Default extends Component {
  render(){
    return (
        <Fragment>
          <div className="row">
            <div className="col-10 mx-auto my-2 text-center text-title">
              <h1 className="text-capitalize font-weight-bold">
                <strong className="text-blue">404!</strong>
                <p>Page Not found!</p>
              </h1>
            </div>
          </div>    
        </Fragment>
      )
  }
}

export default Default;
