import React, { Fragment } from 'react'

export default function Title({title}){
  return(
    <Fragment>
      <div className="row">
        <div className="col-10 mx-auto my-2 text-center text-title">
          <h1 className="text-capitalize font-weight-bold">
            <strong className="text-blue">{title}</strong>
          </h1>
        </div>
      </div>    
    </Fragment>
  )
}
