import React, { Component, Fragment } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';

// load Components
import Navbar from "./components/Navbar";
import Products from "./components/Products";
import ProductDetails from "./components/ProductDetails";
import Cart from "./components/Cart";
import Modal from "./components/Modal";
import Default from "./components/Default";
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Navbar />
          <Switch>
            <Route path="/" component={Products} exact />
            <Route path="/details" component={ProductDetails} />
            <Route path="/cart" component={Cart} />
            <Route component={Default} />
          </Switch>  
          <Modal />          
        </Fragment>
      </BrowserRouter>      
    )
  }
}

export default App;
