import React from 'react';
import ReactDOM from 'react-dom';
import { ProductsProvider } from "./context";
import * as serviceWorker from './serviceWorker';
import './index.css';

import App from './App';

const jsx = (
  <ProductsProvider>
    <App />
  </ProductsProvider>
);
ReactDOM.render(jsx, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
