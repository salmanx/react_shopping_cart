import React, { Component } from 'react'
import { productsData, detailProduct } from "./data";

const ProductContext = React.createContext();
const ProductsConsumer = ProductContext.Consumer;

class ProductsProvider extends Component{

  state = {
    products: [],
    detailProduct: detailProduct,
    carts: [],
    openModal: false,
    modalProduct: detailProduct,
    cartTax: 10,
    cartSubtotal: 10,
    cartTotal: 10
  }

  setProducts = () => {
    let products = [];

    productsData.forEach((product) => {
      let singleProduct = {...product};
      products = [...products, singleProduct]
    });

    this.setState(() => {
      return {
        products: products
      }
    })
  }

  setProductDetails = (id) => {
    return this.state.products.find(product => product.id === id);
  }

  addToCart = (id) => {
    let tmpProducts = [...this.state.products];
    let product = tmpProducts.find((product) => product.id === id);
    product.count = 1;
    const price = product.price;
    product.total = price;
    product.inCart = true;

    this.setState(() => (
      {
        products: tmpProducts,
        carts: [...this.state.carts, product]
      }
    ), () => this.addTotal())
  }

  addTotal = () => {
    let subTotal = 0;
    this.state.carts.map(item => subTotal += item.total );
    const tmpTax = subTotal * 0.01;
    const tax = parseFloat(tmpTax.toFixed(2));
    const total = subTotal + tax;
    this.setState(() => ({
      cartTax: tax,
      cartSubtotal: subTotal,
      cartTotal: total 
    }))

  }

  openModal = (id) => {
    let product = this.setProductDetails(id);
    this.setState(() => {
      return {
        openModal: true,
        modalProduct: product
      }
    })
  }
  
  closeModal = () => {
    this.setState(() => {
      return{
        openModal: false
      }
    })
  }

  componentDidMount() {
    this.setProducts();
  }

  handleDetail = (id) => {
    let product = this.setProductDetails(id);
    this.setState(() => {
      return { detailProduct: product }
    });
  }

  handleAddToCart = (id) => {
    this.addToCart(id);
  }  

  handleIncrementItem = (id) => {
    let tmpCarts = this.state.carts;
    let selectedProduct = tmpCarts.find((p) => p.id === id);
    selectedProduct.count += 1;
    selectedProduct.total = selectedProduct.count * selectedProduct.price;

    this.setState(() => ( { carts: [...tmpCarts] } ), () => this.addTotal() );
  }

  handleDecrementItem = (id) => {
    let tmpCarts = this.state.carts;
    let selectedProduct = tmpCarts.find((p) => p.id === id);
    selectedProduct.count -= 1;

    if (selectedProduct.count === 0) {
      this.handleRemoveItem(id);
    } else {
      selectedProduct.total = selectedProduct.count * selectedProduct.price;
      this.setState(() => ( { carts: [...tmpCarts] } ), () => this.addTotal() );      
    }
  }
  
  handleRemoveItem = (id) => {
    let tmpProducts = [...this.state.products];
    let tmpCarts = [...this.state.carts];
    tmpCarts = tmpCarts.filter(item => item.id !== id);

    const index = tmpProducts.indexOf(this.setProductDetails(id));
    
    let removedProduct = tmpProducts[index];
    removedProduct.inCart = false;
    removedProduct.count = 0;
    removedProduct.total = 0;

    this.setState(
      () => {
        return {
          products: [...tmpProducts],
          carts: [...tmpCarts]
        }
      },
      () => this.addTotal()
    )
  }
  
  handleClearCart = () =>{
    this.setState(
      () => {
        return { carts: [] }
      },
      () => {
        this.setProducts();
        this.addTotal();
      }
    )
  }

  render(){
    return(
      <ProductContext.Provider value={{ 
        ...this.state,
        handleDetail: this.handleDetail,
        handleAddToCart: this.handleAddToCart,
        handleOpenModal: this.openModal,
        handleCloseModal: this.closeModal,
        incrementItem: this.handleIncrementItem,
        decremenItem: this.handleDecrementItem,
        removeItem: this.handleRemoveItem,
        clearCart: this.handleClearCart
      }}>
        {this.props.children}
      </ProductContext.Provider>
    )
  }
}

export { ProductsConsumer, ProductsProvider }

